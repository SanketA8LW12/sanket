import React from 'react'
import { useState, useEffect } from 'react'
const { VITE_KEY, VITE_TOKEN } = import.meta.env;
import axios from "axios";
import Header from './component/header/Header'
import Board from './component/board/Board'

import {
  Route,
  Routes
} from "react-router-dom";
import BoardList from './component/card/BoardList';



function App() {

  const [boardList, setBoardList] = useState([]);
  const [isLoaded, setIsLoaded] = useState(false);
  const [error, setError] = useState('');



  const baseURL = `https://api.trello.com/1/members/me/boards?key=${VITE_KEY}&token=${VITE_TOKEN}`;

  useEffect(() => {
    axios.get(baseURL)
      .then((response) => {
        return response.data
      }).then((data) => {
        setBoardList(data);
        setIsLoaded(true);
      }).catch((error) => {
        console.error(error.message);
        setError(error.message);
      })
  }, [])

  // console.log(board[0]);

  return (
    <>
      < Header />
      {/* <marquee><h1>Happy Friendship Day</h1></marquee> */}

      <Routes>

        <Route path="/" element={
          <>
            <Board boardList={boardList} setBoardList={setBoardList}/>
          </>
        }>
        </Route>
        <Route path="/boardList/:id" element={

          <>
            <BoardList />
          </>
        }>
        </Route>
      </Routes >
    </>
  )
}

export default App
