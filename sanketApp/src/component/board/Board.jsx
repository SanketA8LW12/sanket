// import React from 'react'

// export default function Board() {
//   return (
//     <div>Board</div>
//   )
// }

// import * as React from 'react';
// import Box from '@mui/material/Box';
// import Card from '@mui/material/Card';
// import CardActions from '@mui/material/CardActions';
// import CardContent from '@mui/material/CardContent';
// import Button from '@mui/material/Button';
// import Typography from '@mui/material/Typography';

// const bull = (
//     <Box
//         component="span"
//         sx={{ display: 'inline-block', mx: '2px', transform: 'scale(0.8)', backgroundColor: "blue", }}
//     >
//         •
//     </Box>
// );

// const card = (
//     <React.Fragment>
//         <CardContent sx={{
//             width: "15em", height: "10em",
//             backgroundImage: "url(https://images.pexels.com/photos/260877/pexels-photo-260877.jpeg?auto=compress&cs=tinysrgb&w=600)", backgroundSize: "cover"
//         }}>

//         </CardContent>
//         <CardActions >
//             <Button size="small">DMIETR</Button>
//         </CardActions>
//     </React.Fragment>
// );

// export default function Board({ boardList }) {
//     return (
//         {
//             boardList.map((item) => {
//                 return (
//                     <Box sx={{ width: "15em", height: "10em" }}>
//                         <Card variant="outlined">{card}</Card>
//                     </Box>
//                 )
//             })
//         }
//     );
// }


import React from 'react'
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import axios from 'axios';
import { Card, CardContent, Typography } from '@mui/material';
import { Link } from 'react-router-dom';
const { VITE_KEY, VITE_TOKEN } = import.meta.env;


export default function Board({ boardList, setBoardList }) {

    const cardStyle = {
        backgroundColor: "lightgrey",
        backgroundImage: "url(https://images.pexels.com/photos/2238886/pexels-photo-2238886.jpeg?auto=compress&cs=tinysrgb&w=600)",
        backgroundSize: "cover",
        color: "white",
        borderRadius: "0.4em",
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        border: "3px solid black",
        width: "14em",
        height: "8em",
    };

    const cardStyle1 = {
        backgroundColor: "lightgrey",
        backgroundImage: "url(https://images.pexels.com/photos/2238886/pexels-photo-2238886.jpeg?auto=compress&cs=tinysrgb&w=600)",
        backgroundSize: "cover",
        color: "white",
        borderRadius: "0.4em",
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        border: "3px solid black",
        width: "14em",
        height: "6em",
    };

    const [textValue, setTextValue] = React.useState('');

    const handleChange = (name) => {
        setTextValue(name);
    };

    async function handleClick(name) {
        const url = 'https://api.trello.com/1/boards';
        let newBoard = await axios.post(`${url}?name=${name}&key=${VITE_KEY}&token=${VITE_TOKEN}`);
        setBoardList([...boardList, newBoard])
    }



    return (
        <div>
            <div style={{ display: "flex", gap: "1em", flexWrap: "wrap" , justifyContent: "space-evenly", paddingTop: "1em"
        , backgroundColor: "lightgray"
        }}>
                {boardList.map((data) => {
                    return <Link to={`/boardList/${data.id}`} style={{ textDecoration: 'none' }} key={data.name}>
                        <Card style={cardStyle} sx={{ display: "flex" }}>
                            <CardContent>
                                <Typography variant="h6">
                                    <b>{data.name}</b>
                                </Typography>
                            </CardContent>
                        </Card>
                    </Link>
                    // console.log(data.name)
                })}


                <Card>
                    <CardContent style={cardStyle1}>
                        <Typography variant="h6">
                            
                            <TextField
                                sx={{ backgroundColor: "white", opacity: 0.6  }} // Set background color to transparent
                                label="Enter Board Name"
                                variant="outlined"
                                onChange={(event) => handleChange(event.target.value)}
                            />

                        </Typography>
                        <Button onClick={() => handleClick(textValue)}
                            variant="contained"
                            size="medium"
                            sx={{
                                width: '9em',
                                mt: 2,
                                marginLeft: "3.5em"
                            }}>

                            Add Board

                        </Button>
                    </CardContent>
                </Card>

            </div>
        </div >
    )
}
