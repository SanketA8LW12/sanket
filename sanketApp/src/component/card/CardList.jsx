// import React from 'react'

// export default function Card() {
//   return (
//     <div>Card</div>
//   )
// }


import { Height, Padding } from '@mui/icons-material';
import React, { useState, useEffect } from 'react'
import { Link } from 'react-router-dom';
import { Card, CardContent, Typography } from '@mui/material';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Fab from '@mui/material/Fab';
import AddIcon from '@mui/icons-material/Add';
const { VITE_KEY, VITE_TOKEN } = import.meta.env;
import axios from "axios";
import CardListData from '../cardData/CardListData';
import { CardActionArea } from '@mui/material';
import CardMedia from '@mui/material/CardMedia';

export default function CardList({ name, id, count }) {

  const [addACard, setAddACard] = useState("");

  const [cardData, setCardData] = useState([]);


  const cardStyle = {
    backgroundColor: "black",
    backgroundSize: "cover",
    color: "white",
    borderRadius: "0.4em",
    display: "flex",
    justifyContent: "center",
    border: "3px solid black",
    minWidth: "15em",
    // flexWrap: "wrap",
  };



  const url = `https://api.trello.com/1/lists/${id}/cards?key=${VITE_KEY}&token=${VITE_TOKEN}`;

  useEffect(() => {
    axios.get(url)
      .then((response) => {
        return response.data
      }).then((trello) => {
        setCardData(trello);
      }).catch((error) => {
        console.error(error);
      })
  },[])

  //function to set name and add new card

  function handleAddACardName(name) {
    setAddACard(name);
  }

  async function handleAddACard(name) {
    const url = 'https://api.trello.com/1/cards';
    let newCard = await axios.post(`${url}?name=${name}&idList=${id}&key=${VITE_KEY}&token=${VITE_TOKEN}`);
    setCardData([...cardData, newCard.data])
  }

  // let imgArr = ["https://lh3.googleusercontent.com/pw/AIL4fc-wREtAbFHIl1mCb2XBqLekOPvVNU_D5myKRS0pFTRekYVhDx4ZsASJEjVrGPJdjQmUWOLuSfSTpWhZsfDBgoLxY3o-SUoV0TrUQlbgwGFOKQC4DwmS7_ECACanRbFeLjzxMNmGllUzbyepQ632UZidhmvGpMlOb_zAmo4Q0BDwV8Td_FFO4JrijLA_ykBi8ERXVuWVGeLnrUdMaff4oiKd7itT2g9Leofui2FBbHdWsL-pY4Lyzacpycs3m_svi-kU8Zuvzocx30zelhK3AalXC1ZzDNbHMt2DnkVXkXB6NLcMoveRtr6iKb_wTEDXXxLAUwUKDrRmyE_fGheOFTC0M0wn8ebfRNULJMmO47cu_mf0Oc75gQOYEsKICwMl5lDV6tpfee_87ZGf8d4Dc420QMhHxJr64mQ9fPv_miuAc3YpfLeLtfsdG4Y7ithk2-FpUZ3WSOe57ue4dzqS7ceGM0AQ-bu6sPX5TmrjwNxCjnNHhnyHo9uMu4FfHE5EqnLMUUk8BvB4Y0HfMEHpcT4Fhs5xZtk41O70EIVQTZ3sNBspCxRDTQnwykw90CXjHXHKicJOPwoL6kA3srvg3WRfTfkFO1THXt_-Z-GVSLztaPWRrL6RiQCwWyD0xOR4dzAkj1fFZBPYtYbIQ8lUPRx3mjFqs8Y2FeAJIqcBfimrYVgxqd9Y34049EbxcO46Hlkvxo3k7ju952hmkKrgXCfS4eKQMxCjPEGEylHK5OehudotK7Oeg9UHFLnDJ8V5qu1XmViuGSrEfs8ISCaCKNsBFkNHTiRrSlJQHNJ29eRGsQSpw2WQoepeEVdU1iXQ7gIr88Es3aKjEAQQmHnDFNyL8-A0cW-1rRq_a89HaE8ZlkZ1UIek2hjehZxtPoOHgW0qPkRPmIzIIPsjpWEKKw=w536-h536-s-no?authuser=0"
  // ]  
 


  return (
    <>
      <div>

        <Card style={cardStyle} key={id}>
          <CardContent>
          

            <div>
              <Card sx={{ maxWidth: 345 }}>
                <CardActionArea>
                  <CardMedia
                    component="img"
                    height="170"
                    // image={bgImage}
                    image="https://lh3.googleusercontent.com/pw/AIL4fc-wREtAbFHIl1mCb2XBqLekOPvVNU_D5myKRS0pFTRekYVhDx4ZsASJEjVrGPJdjQmUWOLuSfSTpWhZsfDBgoLxY3o-SUoV0TrUQlbgwGFOKQC4DwmS7_ECACanRbFeLjzxMNmGllUzbyepQ632UZidhmvGpMlOb_zAmo4Q0BDwV8Td_FFO4JrijLA_ykBi8ERXVuWVGeLnrUdMaff4oiKd7itT2g9Leofui2FBbHdWsL-pY4Lyzacpycs3m_svi-kU8Zuvzocx30zelhK3AalXC1ZzDNbHMt2DnkVXkXB6NLcMoveRtr6iKb_wTEDXXxLAUwUKDrRmyE_fGheOFTC0M0wn8ebfRNULJMmO47cu_mf0Oc75gQOYEsKICwMl5lDV6tpfee_87ZGf8d4Dc420QMhHxJr64mQ9fPv_miuAc3YpfLeLtfsdG4Y7ithk2-FpUZ3WSOe57ue4dzqS7ceGM0AQ-bu6sPX5TmrjwNxCjnNHhnyHo9uMu4FfHE5EqnLMUUk8BvB4Y0HfMEHpcT4Fhs5xZtk41O70EIVQTZ3sNBspCxRDTQnwykw90CXjHXHKicJOPwoL6kA3srvg3WRfTfkFO1THXt_-Z-GVSLztaPWRrL6RiQCwWyD0xOR4dzAkj1fFZBPYtYbIQ8lUPRx3mjFqs8Y2FeAJIqcBfimrYVgxqd9Y34049EbxcO46Hlkvxo3k7ju952hmkKrgXCfS4eKQMxCjPEGEylHK5OehudotK7Oeg9UHFLnDJ8V5qu1XmViuGSrEfs8ISCaCKNsBFkNHTiRrSlJQHNJ29eRGsQSpw2WQoepeEVdU1iXQ7gIr88Es3aKjEAQQmHnDFNyL8-A0cW-1rRq_a89HaE8ZlkZ1UIek2hjehZxtPoOHgW0qPkRPmIzIIPsjpWEKKw=w536-h536-s-no?authuser=0"

                  />
                  <CardContent>
                    <Typography gutterBottom variant="h5" component="div">
                      {name}
                    </Typography>
                    <Typography variant="body2" color="text.secondary">
                      Wishing you a day filled with love, laughter, and the company of all the amazing friends in your life. Happy Friendship Day! 🌟
                    </Typography>
                  </CardContent>
                </CardActionArea>
              </Card>
            </div>

            {cardData.filter((item) => {
              return item.idList === id;
            }).map((data) => {
              return (

                <>
                  <CardListData key={data.name}
                    id={data.id}
                    name={data.name}
                    setCardData={setCardData}
                    cardData={cardData}
                  />
                </>
              )
            })

            }

            <Card>
              <CardContent sx={{
                display: "flex",
                gap: "1em"
              }}>
                <Typography variant="h6">
                  <TextField sx={{
                    height: "1em",
                    width: '8em'
                  }}
                    label="Add a tag"
                    variant="outlined"
                    onChange={(event) => handleAddACardName(event.target.value)} />  {/* name of the card */}

                </Typography>

                <Fab size="medium" color="secondary" aria-label="add"
                  onClick={() => {
                    if (addACard) { handleAddACard(addACard) }
                  }}  >  {/* Add a cards */}
                  <AddIcon />   {/* Add button for the cards */}
                </Fab>
              </CardContent>
            </Card>


          </CardContent>
        </Card>
      </div>
    </>
  )
}


